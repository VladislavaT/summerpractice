#include <stdio.h> 
#include <stdlib.h> 
#include <locale.h> 
#include <string.h> 

int balanceBrackets(char * name) {
	int roundOpen = 0, roundClosed = 0, squareOpen = 0, squareClosed = 0, сurlyOpen = 0, curlyClosed = 0;
	for (int i = 0; i < strlen(name); i++) {
		if (name[i] == '(')
			roundOpen++;
		else if (name[i] == ')')
			roundClosed++;
		else if (name[i] == '[')
			squareOpen++;
		else if (name[i] == ']')
			squareClosed++;
		else if (name[i] == '{')
			сurlyOpen++;
		else if (name[i] == '}')
			curlyClosed++;
	}
	if (roundOpen == 0 && roundClosed == 0 && squareOpen == 0 && squareClosed == 0 && сurlyOpen == 0 && curlyClosed == 0)
		return -1;

	else if (roundOpen == roundClosed && squareOpen == squareClosed && сurlyOpen == curlyClosed)
		return 1;
	else
		return 0;
}
